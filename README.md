# CICD Hand On

## Chuẩn bị

### Tạo repo gitlab

#### Bước 1. Fork repository

Truy cập vào repo và bấm bấm vào `fork` để thực hiện fork repo về repo cá nhân:

![](./images/fork1.png)

#### Bước 2. Tạo nhánh phụ để làm nhánh phát triển (feature)

Sau khi fork repo sự kiện cung cấp về repo cá nhân, kích vào + và chọn new branch để tạo branch mới.

![](./images/fork2.png)

ở mục branch name, điền vào tên của branch feature "thường là feature/tên-branch" sau đó chọn create from develop:

![](./images/fork3.png)

Sau khi tạo nhánh feature, ta sẽ có nhánh feature được tạo từ develop như sau:

![](./images/fork4.png) 

###  Tạo resource

#### Bước 1. Tạo resource sonar

Truy cập vào note đã được gửi cho các bạn và lấy thông tin login vào sonar.

Sau khi truy cập thành công, thực hiện tạo project.

Bấm dấu "+" phía trên cùng bên phải rồi chọn Create new project

![](./images/sonar1.png)

Nhập Project key để đặt tên cho project, mẹo: để tránh trùng project nên đặt tên theo convention như sau: `tenhocvien-project`

![](./images/sonar2.png)

> Lưu lại project key ra notepad để sau tiếp tục cấu hình trong jenkins pipeline.

Sau khi tạo project, sẽ có phần Provide a token, học viên điền vào tên token và bấm gen để sonar tạo token cho project. mẹo: để tránh trùng token name, nên đặt tên theo convention như sau: `tenhocvien-token-name`

![](./images/sonar3.png)

Sẽ có 1 token được hiển thị lên cho người dùng, copy token lại và pate vào notepad để sau cấu hình cho jenkins pipeline.

![](./images/sonar4.png)

đã xong phần chuẩn bị sonar!

#### Bước 2: Tạo resource jenkins

Truy cập vào note đã được gửi cho các bạn ở đầu và lấy thông tin login vào jenkins.

Sau khi login, sẽ vào được giao diện của jenkins, chọn new item để tạo thư mục: 

![](./images/jenkins1.png)

Nhập tên cho thư mục và chọn loại item muốn tạo là Folder. mẹo: để tránh trùng project nên đặt tên theo convention như sau: `tenhocvien-folder-name`

![](./images/jenkins2.png)

Sẽ có một giao diện hiện lên để cấu hình folder, không thực hiện cấu hình gì và chọn save .

![](./images/jenkins3.png)

Sau đó tiếp tục tạo pipeline, đứng trong thư mục mới tạo và chọn new item. Ta sẽ thực hiện tạo 2 pipeline 1 cho CI và 1 cho CD.

![](./images/jenkins4.png)

Chọn loại Item là Pipeline và Đặt tên cho pipeline. mẹo: để tránh trùng project nên đặt tên theo convention như sau: `tenhocvien-pipeline-name`

![](./images/jenkins5.png)

Cũng có 1 giao diện để cấu hình pipeline, nhưng ta sẽ chưa cấu hình luôn mà thực hiện lưu lại pipeline trước đã. 

![](./images/jenkins6.png)

Tương tự như tạo pipeline CI, tiến hành tạo thêm 1 pipeline nữa chạy CD.

![](./images/jenkins7.png)

#### Bước 3: Tạo credential cho sonar

Thực hiện tạo credential cho token sonar đã tạo ở bước 1. 

![](./images/jenkins8.png)

Chọn global và chọn Add credential:

![](./images/jenkins9.png)

Tiếp theo chọn kine là `Secret text`. Tại mục secret cần điền vào credential của sonar mà các bạn đã lưu lại notepad ở bước 1. Sau đó nhập tên cho credential. mẹo: để tránh trùng project nên đặt tên theo convention như sau: `tenhocvien-credential-name`

![](./images/jenkins10.png)

> Lưu lại tên sonar credential và notepad để dùng config pipeline.

### Cấu hình CICD

Truy cập lại pipeline CI và tiến hành cấu hình trigger cho pipeline:

![](./images/jenkins11.png)

Chọn Build Triggers và chọn `Build when a change is pushed to GitLab`, tích chọn theo các option trong hình.

![](./images/jenkins12.png)

Tiếp theo, chọn Advanced để cấu hình secret cho trigger. Kích vào `Generate` để gen 1 secret token. note lại token này sử dụng cho việc config trên gitlab.

![](./images/jenkins13.png)

Tương tự như pipeline CI, đối với pipeline CD cũng thực hiện config trigger tương tự, tuy nhiên khác đôi chút về các option cấu hình:

![](./images/jenkins14.png)

Lưu lại các cấu hình và chuyển sang gitlab để cấu hình trigger. 

### Cấu hình Webhook cho gitlab. 

Vào repo trước đó đã fork về và chọn Settings -> Webhooks:

![](./images/gitlab1.png)

Ta sẽ cấu hình 2 trigger cho webhook, 1 cho CI và 1 cho CD.

Trước tiên sẽ cấu hình trigger cho CI:

![](./images/gitlab2.png)

Các thông tin điền vào như URL, Secret token được lấy từ khi config trigger pipeline trên jenkins. 

![](./images/gitlab3.png)

![](./images/gitlab4.png)

Sau khi config, chọn add webhook để thêm trigger vào project.

Tương tự như thế, ta sẽ cấu hình thêm 1 webhook trigger về jenkins pipeline CD nữa. 

![](./images/gitlab5.png)

Sau khi cấu hình trigger cho gitlab thành công, ta sẽ có 2 trigger như sau:

![](./images/gitlab6.png)

### Cấu hình pipeline

Truy cập thư mục pipeline, ta sẽ có 2 file cấu hình jenkins pipeline tương ứng cho CI và CD. 

![](./images/gitlab7.png)

![](./images/gitlab8.png)

khi config pipeline jenkins, ta sẽ sử dụng nội dung 2 file cho việc cấu hình pipeline. Tạm thời để 2 file đó và chuyển sang jenkins.

Tiếp theo, ta trở lại jenkins, truy cập vào mục cấu hình của pipeline CI và CD:

![](./images/jenkins11.png)

Chuyển đến phần cấu hình pipeline.

![](./images/gitlab9.png)

Trong cấu hình pipeline, paste cấu hình pipeline CI tương ứng với nội dung trong file CI trên gitlab:

![](./images/gitlab10.png)

![](./images/gitlab11.png)

Tương tự với cấu hình pipeline CD trên jenkins, ta cũng paste theo cấu hình pipeline CD trên gitlab:

![](./images/gitlab12.png)

![](./images/gitlab13.png)

Chỉnh sửa cấu hình theo hướng dẫn của mentor và Save.
