#!/bin/bash
function input {
echo "
FPT-Software @DEVOPS TEAM
Connection to Jenkins server in-progress...
_________________________
____________________
________________
___________
______
__
_
............................
Please fill your node name :"
read nodenameID

# echo "What is your OS: ubuntu or centos:"
# read os_type

echo "Please fill your secret key :"
read secretID
echo "Please choose your remote directory :Press "Enter" to keep default directory : "/home/jenkins""
read directoryID

DIRECTORYID=${directoryID:-"/home/jenkins"}
mkdir -p $DIRECTORYID ||
export DIRECTORYID=$DIRECTORYID &&
cd $DIRECTORYID
}

function centos7_install_jenkins_slave {

    # Install Java
    java -version
    if [ $? -eq 0 ]; then
        echo "Java installed"
    else
        echo "Install Java version 11"
        yum install -y java-11-openjdk
    fi

    cd $DIRECTORYID && curl -Ok https://jenkins.az.akawork.io/jnlpJars/agent.jar

    cat > $DIRECTORYID/connect-slave.sh << EOF
#!/bin/bash
cd $DIRECTORYID
java -jar agent.jar -jnlpUrl https://jenkins.az.akawork.io/computer/$nodenameID/slave-agent.jnlp -secret $secretID -workDir "$DIRECTORYID"
EOF

    chmod +x $DIRECTORYID/connect-slave.sh
cat > /etc/systemd/system/jenkins-slave.service << EOF
[Unit]
Description=Jenkins Slave service
After=network.target
[Service]

Type=simple
ExecStart=$DIRECTORYID/connect-slave.sh

[Install]
WantedBy=multi-user.target
EOF

    systemctl daemon-reload &&
    systemctl enable jenkins-slave.service &&
    systemctl start jenkins-slave.service ||

    echo "Added slave service in system.
          jenkins-slave.service is started.
          Connection to Jenkins server is successful!"
}

function ubuntu_install_jenkins_slave {

    # Install Java
    java -version
    if [ $? -eq 0 ]; then
        echo "Java installed"
    else
        echo "Install Java version 11"
        apt install openjdk-11-jdk -y
    fi

    cd $DIRECTORYID && curl -Ok https://jenkins.az.akawork.io/jnlpJars/agent.jar

    cat > $DIRECTORYID/connect-slave.sh << EOF
#!/bin/bash
cd $DIRECTORYID
java -jar agent.jar -jnlpUrl https://jenkins.az.akawork.io/computer/$nodenameID/slave-agent.jnlp -secret $secretID -workDir "$DIRECTORYID"
EOF

    chmod +x $DIRECTORYID/connect-slave.sh
cat > /etc/systemd/system/jenkins-slave.service << EOF
[Unit]
Description=java web service
After=network.target
[Service]

Type=simple
ExecStart=$DIRECTORYID/connect-slave.sh

[Install]
WantedBy=multi-user.target
EOF

    systemctl daemon-reload &&
    systemctl enable jenkins-slave.service &&
    systemctl start jenkins-slave.service ||

    echo "Added slave service in system.
          jenkins-slave.service is started.
          Connection to Jenkins server is successful!"

}

function centos6_install_jenkins_slave {

    # Install Java
    java -version
    if [ $? -eq 0 ]; then
        echo "Java installed"
    else
        echo "Install Java version 11"
        yum install -y java-11-openjdk
    fi

    cd $DIRECTORYID && curl -Ok https://jenkins.az.akawork.io/jnlpJars/agent.jar

    cat > $DIRECTORYID/connect-slave.sh << EOF
#!/bin/bash
cd $DIRECTORYID
java -jar agent.jar -jnlpUrl https://jenkins.az.akawork.io/computer/$nodenameID/slave-agent.jnlp -secret $secretID -workDir "$DIRECTORYID"
EOF
    chmod +x $DIRECTORYID/connect-slave.sh
    cat > /etc/init.d/jenkins-slave << EOF
#!/bin/sh
#
# example start stop daemon for CentOS (sysvinit)
#
# chkconfig: - 64 36
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 2 3 4 6
# Required-Start:
# description: example start stop daemon for CentOS

# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network

# Check that networking is up.
[ "\$NETWORKING" = "no" ] && exit 0

USER="root"
APPNAME="jenkins-slave"
APPBIN="/bin/bash"
APPARGS="$DIRECTORYID/connect-slave.sh"
LOGFILE="/var/log/\$APPNAME/error.log"
LOCKFILE="/var/lock/subsys/\$APPNAME"
PIDFILE="/var/run/\$APPNAME.pid"

LOGPATH="/var/log/\$APPNAME"

start() {
        [ -x $prog ] || exit 5
        [ -d \$LOGPATH ] || mkdir \$LOGPATH
        [ -f \$LOGFILE ] || touch \$LOGFILE

        echo -n "Starting \$APPNAME"
        daemon --pidfile=\${PIDFILE} "\$APPBIN \$APPARGS >>\$LOGFILE &"
        RETVAL=$?
        echo
        [ \$RETVAL -eq 0 ] && touch \$LOCKFILE
        return \$RETVAL
}

stop() {
        echo -n "Stopping \$APPNAME "
        pid=\$(ps xau | grep jnlpUrl | awk '{print \$2}' | awk 'FNR=1 {print}')
        kill \$pid
        RETVAL=\$?
        [ \$RETVAL -eq 0 ] && rm -f \$LOCKFILE \$PIDFILE
        return \$RETVAL
}

restart() {
        stop
        start
}

rh_status() {
        status $prog
}

rh_status_q() {
        rh_status >/dev/null 2>&1
}

case "\$1" in
        start)
            start
        ;;
        stop)
            stop
        ;;
        restart)
            restart
        ;;
        status)
            rh_status
        ;;
        *)
                echo \$"Usage: \$0 {start|stop|status|restart}"
                exit 2
esac
EOF

    chmod a+x /etc/init.d/jenkins-slave
    chkconfig jenkins-slave on
    service jenkins-slave start

}

function check_OS {
    DISTRO=$( cat /etc/*-release | grep NAME | tr [:upper:] [:lower:] | grep -Poi '(ubuntu|centos|fedora)' | uniq )
    # Check RedHat Distro
    if [ -z $DISTRO ]; then
    DISTRO=$( cat /etc/*-release | tr [:upper:] [:lower:] | grep -Poi '(red hat)' | uniq )
    fi
}

function check_centos_version {
    CENTOS_MAJOR=$(cat /etc/centos-release | tr -dc '0-9.'|cut -d \. -f1)
}

function main {
    check_OS
    if [ -z $DISTRO ]; then
        DISTRO='unknown'
    fi
    if [ $DISTRO == 'ubuntu' ]; then
        input
        ubuntu_install_jenkins_slave
    elif [ $DISTRO == 'centos' ]; then
        check_centos_version
        if [ $CENTOS_MAJOR == '7' ]; then
            input
            centos7_install_jenkins_slave
        elif [ $CENTOS_MAJOR == '6' ]; then
            input
            centos6_install_jenkins_slave
        fi
    elif [ $DISTRO == 'fedora' ]; then
        input
        centos7_install_jenkins_slave
    elif [ $DISTRO == 'red hat' ]; then
        input
        centos6_install_jenkins_slave
    elif [ $DISTRO == 'unknown' ]; then
        echo "Your OS is not supported."
    fi
}

# Run main script
main